package com.autonomy.marmot.tangoautonomy;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

/**
 * Created by Logan on 3/9/2015.
 *
 * Primary Interface for Autonomy State Machine
 */
public interface AutonomyState {

    // Declare Arena Constants (In Meters)
    final double arenaX = 3.78;
    final double arenaZ = 7.38;
    final double diggingX = 3.78;
    final double diggingZ = 2.94;
    final double obstacleX = 3.78;
    final double obstacleZ = 2.94;
    final double dumpingX = 3.78;
    final double dumpingZ = 1.50;
    final double safetyFactor = 0.25;

    //Arena Constants for initilization
    final double startingX = 1.89;//the dimensions of the starting location
    final double startingZ = 1.83;
    final double startingZFromDig = 5.49;

    //cardinal directions
    public enum Cardinal {
        NORTH,//towards digging site
        WEST,
        SOUTH,//towards dump site
        EAST
    }


    // Action Method to Process Pose Data and Point Cloud (Navigation)
    void action(TangoPoseData poseData, TangoXyzIjData depthData);

}
