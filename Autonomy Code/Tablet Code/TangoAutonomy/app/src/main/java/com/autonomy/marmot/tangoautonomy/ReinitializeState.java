package com.autonomy.marmot.tangoautonomy;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

/**
 * Created by Logan on 3/9/2015.
 *
 * Reorient to Face Digging Area after Dumping is Complete
 */
public class ReinitializeState implements AutonomyState {

    // Declare Needed Class Variables
    AutonomyControl autonomyControl;

    // Constructor for State Class
    public ReinitializeState(AutonomyControl autonomyControl){
        this.autonomyControl = autonomyControl;
    }

    @Override
    public void action(TangoPoseData poseData, TangoXyzIjData depthData) {

    }
}
