package com.autonomy.marmot.tangoautonomy;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

/**
 * Created by Logan on 3/9/2015.
 *
 * Rotate 90 Degrees to Face Side Wall(s)
 * Lower Digging Mechanism
 * Activate Digging Brush
 * Move Back and Forth While Digging
 */
public class DiggingState implements AutonomyState {

    // Declare Needed Class Variables
    AutonomyControl autonomyControl;

    // Constructor for State Class
    public DiggingState(AutonomyControl autonomyControl){
        this.autonomyControl = autonomyControl;
    }

    @Override
    public void action(TangoPoseData poseData, TangoXyzIjData depthData) {

    }
}
