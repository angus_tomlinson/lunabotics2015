package com.autonomy.marmot.tangoautonomy;

import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

/**
 * Created by Logan on 3/9/2015.
 * Filled out by Alex 3/21/2015
 * <p/>
 * Requests Sensor Data to Determine Initial Orientation Within the Arena. Orient Rover to Face Digging Area.
 */
public class InitializeState implements AutonomyState {


    //left = 1, right = 2, front = 3
    private int leftDist = -1, rightDist = -1, frontDist = -1;//distances retrieved from laser range finders in meters

    //location of the center of the robot in meters
    public double curX = -1, curZ = -1;

    //margin of error for laser readings
    private final double E = .25;

    Cardinal curDir; //the cardinal direction the bot is currently facing
    boolean inLeftPosition; //false is left, true is right


    // Declare Needed Class Variables
    AutonomyControl autonomyControl;

    // Constructor for State Class
    public InitializeState(AutonomyControl autonomyControl) {
        this.autonomyControl = autonomyControl;

    }

    @Override
    public void action(TangoPoseData poseData, TangoXyzIjData depthData) {
        determineInitialPosition();
        rotateNorth(curDir);
        //make sure that current state is then set to NavigateToDigState
    }

    //get the initial x coord after rotation. returns -1 if not yet initialized
    public double getX(){
        return curX;
    }

    //get the initial x coord after rotation. returns -1 if not yet initialized
    public double getZ(){
        return curZ;
    }

    public boolean initializedCorrectly(){
        return (curDir == Cardinal.NORTH && curX != -1 && curZ != -1);
    }

    //note: right now this code assumes that readings are exact
    //TODO: allow margins of error on ranges
    private void determineInitialPosition() {

        //get the ranges from each laser pointer
        getNewRangeData();

        //there are 8 cases: four directions and two starting locations

        if (frontDist > leftDist && frontDist > rightDist) {//if frontDist is the largest value
            curDir = Cardinal.NORTH;//the robot must be pointed towards the digging site

            if (leftDist < rightDist)
                inLeftPosition = true;
            else
                inLeftPosition = false;
        } else if (rightDist > leftDist && rightDist > frontDist) {//if rightDist is the largest value

            if (rightDist < arenaX) {//it must be pointed backwards in the right position
                curDir = Cardinal.SOUTH;
                inLeftPosition = false;
            } else {//it is facing west in one of the two positions
                curDir = Cardinal.WEST;
                if (lessThanWithError(frontDist, startingX, E)) inLeftPosition = true;
                else inLeftPosition = false;
            }
        } else if (leftDist > rightDist && leftDist > frontDist) {//if leftDist is the largest value

            if (leftDist < arenaX) {//it must be pointed backwards in the left position
                curDir = Cardinal.SOUTH;
                inLeftPosition = true;
            } else {//it is facing east in one of the two positions
                curDir = Cardinal.EAST;
                if (lessThanWithError(frontDist, startingX, E)) inLeftPosition = false;
                else inLeftPosition = true;
            }
        }
    }

    //rotate north from the specified direction
    //returns true if it faces north afterwards
    private boolean rotateNorth(Cardinal dir) {
        //rotate
        switch (dir){
            case NORTH:
                //do nothing
                break;
            case EAST:
                rotate(false, 90);
                break;
            case SOUTH:
                rotate(true, 180);
                break;
            case WEST:
                rotate(true, 90);
                break;
        }

        //recalculate position
        calculatePosition();
        //assert that it is facing the correct direction
        return (greaterThanWithError(frontDist, leftDist, E) && greaterThanWithError(frontDist, rightDist, E));

    }

    //returns true if d1 is less than or equal d2 +- e
    public boolean lessThanWithError(double d1, double d2, double e){
        return (d1 <= d2 + e || d1 <= d2 - e);
    }

    //returns true if d1 is greater than or equal d2 +- e
    public boolean greaterThanWithError(double d1, double d2, double e){
        return (d1 >= d2 + e || d1 >= d2 - e);
    }

    //returns true if d1 is within +- e of of d2
    public boolean equalsWithError(double d1, double d2, double e){
        double dd = d1 - d2;
        return Math.abs(dd) < e;
    }

    //assumes robot is facing North
    //assumes robot is a point with no width/depth
    //TODO: add robot size info
    private void calculatePosition() {
        getNewRangeData();
        curX = leftDist;
        curZ = arenaZ - frontDist;
    }

    //rotates robot the specified angle
    //TODO: figure out how to activate motors
    private void rotate(boolean clockwise, int angle) {

    }

    //assigns range values to leftDist, rightDist, frontDist
    //TODO: retrieve data
    private void getNewRangeData() {
        leftDist = 0;
        rightDist = 0;
        frontDist = 0;
    }
}


