package com.autonomy.marmot.tangoautonomy;


// Import Needed Libraries

import android.content.Context;
import android.graphics.Color;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;

import android.os.Handler;

/*
 * Class to Run Autonomy State Machine
 */

public class AutonomyControl implements Runnable {

    // Declare Tango Objects
    public TangoPoseData currentPose;
    public TangoXyzIjData currentXyzIj;
    public TangoEvent currentTangoEvent;

    private Tango mTango;
    private TangoConfig mConfig;

    // Declare Frame Pair List
    ArrayList<TangoCoordinateFramePair> framePairs;

    // Declare States for Autonomy
    private AutonomyState diggingState;
    private AutonomyState dumpingState;
    private AutonomyState initializeState;
    private AutonomyState navigateToDigState;
    private AutonomyState navigateToDumpState;
    private AutonomyState reinitializeState;

    // Declare Current State
    private AutonomyState currentState;

    // Declare Autonomy Control Variables
    private boolean autonomyActive;
    final int AUTONOMY_FRAME_INTERVAL = 10;

    public FloatBuffer mVertexBuffer;
    public int mPointCount;

    /*
     * Constructor Method for Autonomy Control Thread
     */
    public AutonomyControl(Context context, Handler displayHandler) {


        // Initialize Tango Device
        mTango = new Tango(context);

        // Get Configuration from Tango
        mConfig = mTango.getConfig(TangoConfig.CONFIG_TYPE_CURRENT);

        // Enable Motion Tracking, Area Learning, and Depth Perception
        mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_MOTIONTRACKING, true);
        mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_LEARNINGMODE, true);
        mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_DEPTH, true);

        //Initialize Frame Pair List
        framePairs = new ArrayList<>();

        // Initialize Autonomy States
        diggingState = new DiggingState(this);
        dumpingState = new DumpingState(this);
        initializeState = new InitializeState(this);
        navigateToDigState = new NavigateToDigState(this);
        navigateToDumpState = new NavigateToDumpState(this);
        reinitializeState = new ReinitializeState(this);

        // Initialize Current State to First State in Sequence
        currentState = initializeState;


        // Initialize Autonomy Off at Start
        autonomyActive = false;
    }


    /*
     * Method to Enable or Disable Autonomy
     */
    private void setAutonomyActive(boolean autonomyActive) {

        // Update Autonomy Boolean
        this.autonomyActive = autonomyActive;

    }

    /*
     * Main Autonomy Loop Method
     */
    @Override
    public void run() {


    }

    /*
   * Create Listeners to Handle Events from Tango
   */
    private void setTangoListeners() {
        framePairs = new ArrayList<TangoCoordinateFramePair>();
        framePairs.add(new TangoCoordinateFramePair(
                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                TangoPoseData.COORDINATE_FRAME_DEVICE));
        System.out.println("setTangoListeners");

        //listen for new Tango Data
        mTango.connectListener(framePairs, new Tango.OnTangoUpdateListener() {

            @Override
            public void onFrameAvailable(int frame){

            }

            @Override
            public void onPoseAvailable(final TangoPoseData pose) {

                //update the instance variable currentPose
                currentPose = pose;

                // Call Action Method of Current State to Direct Robot
                currentState.action(currentPose, currentXyzIj);


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        System.out.println("Running UI");

                        DecimalFormat threeDec = new DecimalFormat("0.000");
                        //print translation
                        String translationString = "Translation from Base: ["
                                + threeDec.format(pose.translation[0]) + ", "
                                + threeDec.format(pose.translation[1]) + ", "
                                + threeDec.format(pose.translation[2]) + "]";
                        translationDisplay.setText(translationString);

                        //print quaternions
//                        String quaternionString= "Quaternions: ["
//                                + threeDec.format(pose.rotation[0]) + ", "
//                                + threeDec.format(pose.rotation[1]) + ", "
//                                + threeDec.format(pose.rotation[2]) + ", "
//                                + threeDec.format(pose.rotation[3]) + "]";
//                        quaternionDisplay.setText(quaternionString);

                        //print rotation
                        double[] conversion = quaternionToEuler(pose.rotation);
                        String rotationString = "Rotation: ["
                                + threeDec.format(conversion[0]) + ", "
                                + threeDec.format(conversion[1]) + ", "
                                + threeDec.format(conversion[2]) + "]";
                        rotationDisplay.setText(rotationString);

                        //print theta
//                        double theta = theta(conversion);
//                        String thetaString =  "Theta: " + threeDec.format(theta);
//                        thetaText.setText(thetaString);

                        if (pose.statusCode == TangoPoseData.POSE_VALID) {
                            poseStatus.setText("POSE VALID");
                        } else if (pose.statusCode == TangoPoseData.POSE_INVALID) {
                            poseStatus.setText("POSE INVALID");
                        } else if (pose.statusCode == TangoPoseData.POSE_INITIALIZING) {
                            poseStatus.setText("POSE INITIALIZING");
                        } else if (pose.statusCode == TangoPoseData.POSE_UNKNOWN) {
                            poseStatus.setText("UNKNOWN POSE");
                        }
                    }
                });
            }

            @Override
            public void onXyzIjAvailable(TangoXyzIjData tangoXyzIjData) {
                //update instance variable
                currentXyzIj = tangoXyzIjData;
            }

            @Override
            public void onTangoEvent(TangoEvent tangoEvent) {
//                //update instance variable
                currentTangoEvent = tangoEvent;
            }
        });
    }


    //code that *should* translate an quaternion q1 into a rotation vector, which it returns
    public double[] quaternionToEuler(double[] q1) {

        //compute length of quaternion
        double length = Math.sqrt(q1[0] * q1[0] + q1[1] * q1[1] + q1[2] * q1[2] + q1[3] * q1[3]);

        //normalize quaternion
        double x = q1[0] / length;
        double y = q1[1] / length;
        double z = q1[2] / length;
        double w = q1[3] / length;

        //compute angle
        double angle = 2 * Math.acos(w);
        double s = Math.sqrt(1 - w * w); // assuming quaternion normalized then w is less than 1, so term always positive.
        if (s < 0.001) { // test to avoid divide by zero, s is always positive due to sqrt
            return new double[]{x, y, z};
        } else {
            double rx = x / s; // normalize axis
            double ry = y / s;
            double rz = z / s;
            return new double[]{rx, ry, rz};
        }
    }

    //returns the angle of angle of the tango with respect to its original
    //TODO: Test
    public double theta(double[] q) {
        double x = q[0];
        double w = q[3];
        return x / Math.sin(Math.acos(w));
//        return Math.atan(Math.sqrt(x * x + y * y)/z);
    }
}