package com.autonomy.marmot.tangoautonomy;

/**
 * Created by Joe on 10/27/2015.
 */
public class PowerFinder {
    public double[] getPower(DoublePoint tangoLocation, DoublePoint destination, double tangoOrientation){
        //tangoOrientation is the angle between the direction Tango is facing, and the direction of the destination
        double[] motorPowers = {0.0,0.0};

        if(tangoLocation.distance(destination) > 1){//distance assumed to be in meters
            if(tangoOrientation > 10 && tangoOrientation < 180){
                double x = (tangoOrientation-10)/(17/5) + 50;
                motorPowers[0] = -x;
                motorPowers[1] = x;
            }else if(tangoOrientation < 350 && tangoOrientation >= 180){
                double x = (tangoOrientation-180)/(17/5) + 50;
                motorPowers[0] = x;
                motorPowers[1] = -x;
            }else if(tangoOrientation < 10){
                motorPowers[0] = 50-tangoOrientation;
                motorPowers[1] = 50+tangoOrientation;
            }else{
                motorPowers[0] = 50-(360-tangoOrientation);
                motorPowers[1] = 50+(360-tangoOrientation);
            }
        }else{
            return pointAndShoot(tangoOrientation);
        }



        return motorPowers;
    }

    public double[] pointAndShoot(double tangoOrientation){
        double[] motorPowers = {0.0,0.0};
        if(tangoOrientation > 10 && tangoOrientation < 180){
            motorPowers[0] = -50;
            motorPowers[1] = 50;
        }else if(tangoOrientation < 350 && tangoOrientation >= 180){
            motorPowers[0] = 50;
            motorPowers[1] = -50;
        }else{
            motorPowers[0] = 50;
            motorPowers[1] = 50;
        }
        return motorPowers;
    }
}
