package com.autonomy.marmot.tangoautonomy;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoCameraIntrinsics;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static com.google.atap.tangoservice.TangoCameraIntrinsics.*;

/**
 * Created by Logan on 3/9/2015.
 *
 * Navigate Across Obstacle Field to Target
 */
public class NavigateToDigState implements AutonomyState {

    // Declare Needed Class Variables
    AutonomyControl autonomyControl;

    // Constructor for State Class
    public NavigateToDigState(AutonomyControl autonomyControl){
        this.autonomyControl = autonomyControl;
    }

    @Override
    public void action(TangoPoseData poseData, TangoXyzIjData depthData) {
        // receive the pointCloud
        byte[] buffer = new byte[depthData.xyzCount * 3 * 4];
        FileInputStream fileStream = new FileInputStream(
                depthData.xyzParcelFileDescriptor.getFileDescriptor());
        try {
            fileStream.read(buffer, depthData.xyzParcelFileDescriptorOffset, buffer.length);
            fileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FloatBuffer mPointCloudFloatBuffer;
        mPointCloudFloatBuffer = ByteBuffer.wrap(buffer)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        autonomyControl.mPointCount = depthData.xyzCount;
        autonomyControl.mVertexBuffer.clear();
        autonomyControl.mVertexBuffer.position(0);
        autonomyControl.mVertexBuffer.put(mPointCloudFloatBuffer);

        // camera's translation
        double xC = poseData.INDEX_TRANSLATION_X;
        double yC = poseData.INDEX_TRANSLATION_Y;
        double zC = poseData.INDEX_TRANSLATION_Z;

        // camera's rotation
        double qX = poseData.INDEX_ROTATION_X;
        double qY = poseData.INDEX_ROTATION_Y;
        double qZ = poseData.INDEX_ROTATION_Z;
        double qW = poseData.INDEX_ROTATION_W;

        // point's coordinates
        double x;
        double y;
        double z;

        // screen coordinates
        int u;
        int v;

        // camera intrinsics
        TangoCameraIntrinsics intrinsics = new TangoCameraIntrinsics();
        // focal length
        double fX = intrinsics.fx;
        double fY = intrinsics.fy;
        // pin-point's position in the view plane
        double cX = intrinsics.cx;
        double cY = intrinsics.cy;
        // scaling between the x- and y-axis (typically set to zero)
        double s = 0;
        // initialize screen array
        double [][] screen = new double[2 * (int) cY][2 * (int) cX];

        // process the pointCloud and render z-values to the screen array
        for (int i = 0; i < mPointCloudFloatBuffer.capacity() - 3; i = i + 3) {
            x = mPointCloudFloatBuffer.get(i);
            y = mPointCloudFloatBuffer.get(i + 1);
            z = mPointCloudFloatBuffer.get(i + 2);

            // initialize rotation matrix
            double r11 = (1 - (2 * qY * qY) - (2 * qZ * qZ));
            double r12 = ((2 * qX * qY) - (2 * qW * qZ));
            double r13 = ((2 * qX * qZ) + (2 * qW * qY));
            double r21 = ((2 * qX * qY) + (2 * qW * qZ));
            double r22 = (1 - (2 * qX * qX) - (2 * qZ * qZ));
            double r23 = ((2 * qY * qZ) - (2 * qW * qX));
            double r31 = ((2 * qX * qZ) - (2 * qW * qY));
            double r32 = ((2 * qY * qZ) + (2 * qW * qX));
            double r33 = (1 - (2 * qX * qX) - (2 * qY * qY));

            double[][] rotationMatrix = {{r11, r12, r13}, {r21, r22, r23}, {r31, r32, r33}};

            // transformation vector
            double t1 = -((r11 * xC) + (r12 * yC) + (r13 * zC));
            double t2 = -((r21 * xC) + (r22 * yC) + (r23 * zC));
            double t3 = -((r31 * xC) + (r32 * yC) + (r33 * zC));

            // extrinsic matrix
            double[][] extrinsicMatrix = {{r11, r12, r13, t1}, {r21, r22, r23, t2}, {r31, r32, r33, t3}};



            // intrinsic matrix
            double[][] intrinsicMatrix = {{fX, s, cX}, {0, fY, cY}, {0, 0, 1}};

            // camera matrix's individual components
            double c11 = ((r11 * fX) + (r21 * s) + (r31 * cX));
            double c12 = ((r12 * fX) + (r22 * s) + (r32 * cX));
            double c13 = ((r13 * fX) + (r23 * s) + (r33 * cX));
            double c14 = ((t1 * fX) + (t2 * s) + (t3 * cX));
            double c21 = ((r21 * fY) + (r31 * cY));
            double c22 = ((r22 * fY) + (r32 * cY));
            double c23 = ((r23 * fY) + (r33 * cY));
            double c24 = ((t2 * fY) + (t3 * cY));
            double c31 = r31;
            double c32 = r32;
            double c33 = r33;
            double c34 = t3;

            // camera matrix
            double[][] cameraMatrix = {{c11, c12, c13, c14}, {c21, c22, c23, c24}, {c31, c32, c33, c34}};

            // camera matrix multiplied by the homogeneous 3D point vector (PX)
            double[] finalMatrix = {((c11 * x) + (c12 * y) + (c13 * z) + c14), ((c21 * x) + (c22 * y) + (c23 * z) + c24),
                    ((c31 * x) + (c32 * y) + (c33 * z) + c34)};

            // u, v coordinates on the screen;
            u = (int) (finalMatrix[0] / finalMatrix[2]);
            v = (int) (finalMatrix[1] / finalMatrix[2]);

            // initializes z-value to the screen 2d array
            screen[u][v] = distance(xC, yC, zC, mPointCloudFloatBuffer.get(i), mPointCloudFloatBuffer.get(i + 1), mPointCloudFloatBuffer.get(i + 2));
        }
    }

    // calculates the distance between the camera and the point
    public double distance(double x1, double y1, double z1, double x2, double y2, double z2){
        double distance = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)) + ((z2 - z1) * (z2 - z1)));
        return distance;
    }
}
