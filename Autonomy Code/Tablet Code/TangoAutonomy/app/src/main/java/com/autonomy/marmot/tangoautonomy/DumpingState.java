package com.autonomy.marmot.tangoautonomy;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

/**
 * Created by Logan on 3/9/2015.
 *
 * Localize Dumping Bin
 * Move to Bin While Lifting Bucket
 * Dump Until Empty
 */
public class DumpingState implements AutonomyState {

    // Declare Needed Class Variables
    AutonomyControl autonomyControl;

    // Constructor for State Class
    public DumpingState(AutonomyControl autonomyControl){
        this.autonomyControl = autonomyControl;
    }

    @Override
    public void action(TangoPoseData poseData, TangoXyzIjData depthData) {

    }
}
