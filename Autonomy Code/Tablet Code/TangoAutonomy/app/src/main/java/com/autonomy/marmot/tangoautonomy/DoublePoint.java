package com.autonomy.marmot.tangoautonomy;

/**
 * Created by Joe on 10/27/2015.
 */
public class DoublePoint {
    double x,y;
    public DoublePoint(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double distance(DoublePoint dest){
        double squaredDist = Math.pow(x-dest.x,2) + Math.pow(y-dest.y,2);
        return Math.sqrt(squaredDist);
    }
}
