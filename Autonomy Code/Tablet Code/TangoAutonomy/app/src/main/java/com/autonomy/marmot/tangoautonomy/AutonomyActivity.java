package com.autonomy.marmot.tangoautonomy;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.atap.tangoservice.Tango;

/*
 * Class to Run Autonomy State Machine
 */

public class AutonomyActivity extends ActionBarActivity {

    // Declare Display Objects
    private TextView autonomyStatus;
    private TextView translationDisplay;
    private TextView rotationDisplay;
    private TextView poseStatus;
    private Spinner manualStateList;
    private Button setStateButton;
    private ToggleButton autonomyToggleButton;

    // Declare Main Autonomy Control Object and Thread
    AutonomyControl autonomyMainControl;
    Thread autonomyControlThread;

    // Declare Display Handler
    Handler displayHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autonomy);

        // Request User Permission for Tango Services
        startActivityForResult(Tango.getRequestPermissionIntent(Tango.PERMISSIONTYPE_MOTION_TRACKING), Tango.TANGO_INTENT_ACTIVITYCODE);
        startActivityForResult(Tango.getRequestPermissionIntent(Tango.PERMISSIONTYPE_ADF_LOAD_SAVE), Tango.TANGO_INTENT_ACTIVITYCODE);

        // Initialize Display Objects
        autonomyStatus = (TextView) findViewById(R.id.autonomyStatus);
        manualStateList = (Spinner) findViewById(R.id.manualStateList);
        setStateButton = (Button) findViewById(R.id.setStateButton);
        autonomyToggleButton = (ToggleButton) findViewById(R.id.autonomyToggleButton);
        translationDisplay = ((TextView) findViewById(R.id.tangoXData));
        rotationDisplay = ((TextView) findViewById(R.id.tangoXRotationData));
        poseStatus = ((TextView) findViewById(R.id.poseStatus));

        // Populate List for Manual Set State
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.autonomy_select_state, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        manualStateList.setEnabled(false);
        manualStateList.setAdapter(adapter);

        // Initialize Autonomy Control Thread
        autonomyMainControl = new AutonomyControl(this, displayHandler);
        autonomyControlThread = new Thread(autonomyMainControl, "Autonomy");
        autonomyControlThread.start();

        // Initialize Handler for Updating Display Object from Autonomy Thread
        displayHandler = new Handler(getMainLooper()){
            @Override
            public void handleMessage(Message inputMessage){

                // Code for Message Handling
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_autonomy, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
     * Method to Update Autonomy Status Display
     */
    private void setAutonomyDisplay(boolean autonomyActive) {

        // Update Current State and Display Objects Based on Boolean Value
        if (autonomyActive) {

            // Set Autonomy Status Text
            autonomyStatus.setText("Active");
            autonomyStatus.setTextColor(Color.parseColor("#FF00C800"));

            // Enabled Set State Display Objects
            manualStateList.setEnabled(true);
            setStateButton.setEnabled(true);

        } else {

            // Set Autonomy Status Text
            autonomyStatus.setText("Inactive");
            autonomyStatus.setTextColor(Color.RED);

            // Disable Set State Display Objects
            manualStateList.setEnabled(false);
            setStateButton.setEnabled(false);

        }
    }


    /*
     * Manually Enable/Disable Autonomy for Testing Purposes
     */
    public void handleAutonomyToggleButton(View view) {

        // If Toggled On, Enable Autonomy
        if (autonomyToggleButton.isChecked()) {
            //setAutonomyActive(true);
        }

        // Else Disable Autonomy on Button Press
        else {
            //setAutonomyActive(false);
        }
    }

    /*
     * Manually Set Autonomy State for Testing Purposes
     */
    public void handleSetStateButton(View view) {

        // Set Text
        TextView autonomyState = (TextView) findViewById(R.id.autonomyState);
        String newState = manualStateList.getSelectedItem().toString();
        autonomyState.setText(newState);

    }
}
