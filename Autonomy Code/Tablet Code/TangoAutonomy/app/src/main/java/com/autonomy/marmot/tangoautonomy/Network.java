package com.autonomy.marmot.tangoautonomy;

import android.os.Handler;

import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by Logan on 3/11/2015.
 * <p/>
 * Thread to Control Network Communication with Project Tango
 */
public class Network {

    // Declare Needed Class Variables
    private DatagramSocket socket;
    private volatile boolean autonomy;
    private volatile boolean exitFlag;
    private boolean newData;
    private Handler tangoHandler;



    /*
     * Constructor for Network Object
     */
    public Network(int portNumber, Handler tangoHandler) {

        // Connect Socket
        networkConnect(portNumber);

        // Initialize AutonomyActivity, Exit Flag, and New Data Flag to False
        autonomy = false;
        exitFlag = false;
        newData = false;

        // Initialize Robot Handler to Pass Commands
        this.tangoHandler = tangoHandler;
    }


    /*
     * Method to Connect to Control Client over UDP Socket
     */
    public void networkConnect(int portNumber) {

        // Initialize Socket
        try {
            socket = new DatagramSocket(portNumber);
        } catch (SocketException ex) {
            System.out.println("Cannot Create Socket!");
        }
    }

    /*
    * Network Packet Structure
    * [0] = Left Side Wheels (-100 - 100)
    * [1] = Right Side Wheels (-100 - 100)
    * [2] = Digging Brush Toggle (0 or 1)
    * [3] = Brush Lift (0 OFF, 1 UP, 2 DOWN)
    * [4] = Bucket Lift (0 OFF, 1 UP, 2 DOWN)
    * [5] = Bucket Pitch (0 OFF, 1 FORWARDS, 2 BACKWARDS)
    * ------------------------------------
    * [6] = Autonomy Status (0 OFF, 1 ON, 2 NORTH, 3 SOUTH, 4 EAST, 5 WEST)
    * [7] = Exit Flag (0 NO EXIT, 1 EXIT)
    */

    /*
     * Method to Send Movement Commands
     */

}
