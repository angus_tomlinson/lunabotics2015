package com.lunabotics.controller.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by Logan on 3/20/14.
 * <p/>
 * Thread to Control Network Communication with Project Tango
 */
public class Autonomy implements Runnable {

    // Declare Tango IP Address
    private String tangoIP;

    // Declare Class Variables
    private DatagramSocket socket;
    private double weightCollected;
    private Handler robotHandler;
    private boolean autonomy;
    private InetAddress tangoAddress;
    private int tangoPort;

    /*
     * Constructor for Autonomy Object
     */
    public Autonomy(int portNumber, Handler robotHandler) {

        // Set Tango IP Address (CONFIGURE STATIC IP ON ROUTER)
        tangoIP = "192.168.1.105";

        // Connect Socket
        autonomyConnect(portNumber);

        // Initialize Weight Collected Variable to Zero to Display
        weightCollected = 0;

        // Initialize Handler for Robot Commands
        this.robotHandler = robotHandler;

        // Set Autonomy Boolean
        autonomy = false;

        // Initialize Tango Address
        try {
            tangoAddress = InetAddress.getByName(tangoIP);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        // Set Tango Port
        tangoPort = portNumber;

        // Set Tango IP Address (CONFIGURE STATIC IP ADDRESS ON ROUTER)


    }

    /*
     * Method to Initialize and Deactivate Autonomy on Tango
     */
    public void autonomyToggle(boolean autonomyState, int init) {

        // If Autonomy has been Activated
        if (autonomyState) {

            if (init == 1) {
                // Send Activation Signal to Tango
                autonomySend("A");
            } else if (init == 2){
                autonomySend("N");
            } else if (init == 3){
                autonomySend("S");
            } else if (init == 4){
                autonomySend("E");
            } else if (init == 5){
                autonomySend("W");
            }
        }

        // If Autonomy has been Deactivated
        if (!autonomyState) {

            // Send Deactivation Signal to Tango
            autonomySend("D");
        }

        // Set Autonomy Boolean for Run Loop
        autonomy = autonomyState;
    }

    /*
     * Method to Connect to Control Client over UDP Socket
     */
    public void autonomyConnect(int portNumber) {

        // Initialize Socket
        try {
            socket = new DatagramSocket(portNumber);
        } catch (SocketException ex) {
            System.out.println("Cannot Create Socket!");
        }
    }

    /*
     * Main Execution Cycle for Thread
     */
    @Override
    public void run() {

        // Loop Autonomy Functionality Indefinitely
        while (true) {

            // Only Execute While Autonomy is Active
            if (autonomy) {

                // Get Data from Socket
                String autonomyData = autonomyReceive();

                // Parse Data into Commands
                int[] autonomyCommands = autonomyParse(autonomyData);

                // Create Bundle to Send with Capacity Size Two
                Bundle sendData = new Bundle(2);

                // Add Elements to Bundle
                sendData.putString("ID", "Autonomy");
                sendData.putIntArray("DATA", autonomyCommands);

                // Send New Data Through Handler
                Message commandMessage = robotHandler.obtainMessage();
                commandMessage.obj = sendData;
                commandMessage.sendToTarget();
            }
        }
    }

    /*
    * Receive Packet Over UDP Socket
    */
    private String autonomyReceive() {

        // Declare Receiving Packet and Data Buffer
        DatagramPacket packet;
        byte[] packetBuffer = new byte[128];

        packet = new DatagramPacket(packetBuffer, packetBuffer.length);

        // Try-Catch Statement to Receive Data Over Socket
        try {
            socket.receive(packet);
        } catch (IOException ex) {
            System.out.println("No Data Received Over UDP Socket");
        }

        // Receive Socket Data Into Buffer
        byte[] received = new byte[packet.getLength() - 1];
        received = packet.getData();

        // Convert Received Data to String and Remove Empty Characters
        String data = new String(received);
        data = data.replaceAll("\u0000", "");

        // Return String
        return data;
    }

    /*
   * Parse String Data into Commands
   */
    private int[] autonomyParse(String data) {

        /*
         * Network Packet Structure
         * [0] = Left Side Wheels (-100 - 100)
         * [1] = Right Side Wheels (-100 - 100)
         * [2] = Digging Wheel Toggle (0 or 1)
         * [3] = Wheel Actuator (0 OFF, 1 UP, 2 DOWN)
         * [4] = Scissor Lift (0 OFF, 1 UP, 2 DOWN)
         * [5] = Conveyor Belt (0 OFF, 1 FORWARDS, 2 BACKWARDS)
         * ------------------------------------
         * [6] = Weight Collected (Divide by 100 for Measurement in Kilograms)
         */

        // Initialize Network Commands
        int[] networkCommands = new int[7];

        // Split Data String into Array by Space
        String[] split = data.split("\\ ");

        // Convert String Array to Int Array
        for (int i = 0; i < split.length; i++) {
            networkCommands[i] = Integer.parseInt(split[i]);
        }

        // Return Parsed Network Commands
        return networkCommands;
    }

    /*
     * Send Commands to Tango
     */
    public void autonomySend(String sendString) {

        // Send Data to Tango
        byte[] sendData = sendString.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, tangoAddress, tangoPort);
        try {
            socket.send(sendPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
