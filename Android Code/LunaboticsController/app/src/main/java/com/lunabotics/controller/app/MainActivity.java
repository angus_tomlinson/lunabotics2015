package com.lunabotics.controller.app;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.net.*;
import java.util.*;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

    }

    /*
     * Begin Control Code
     * Create Handler for Button Press
     */

    public void robotConnect(View view) {

        // Disable Button
        Button connectButton = (Button) findViewById(R.id.button);
        connectButton.setEnabled(false);

        // Declare TextView Objects to Print
        final TextView textStatus = (TextView) findViewById(R.id.textViewStaus);
        textStatus.setMovementMethod(new ScrollingMovementMethod());

        textStatus.append("Connecting to Socket...\n");

        // Declare Handler to Receive Messages
        Handler robotHandler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message inputMessage) {

                // Get Display String from Incoming Message Object
                String displayMessage = (String) inputMessage.obj;

                // Append Message to Status
                textStatus.append(displayMessage + "\n");
            }
        };

        // Declare Main Robot Object and Start Thread
        Robot robot = new Robot(this, robotHandler);
        Thread robotThread = new Thread(robot, "Robot");
        robotThread.start();

        // Create TextView for IP
        TextView textIP = (TextView) findViewById(R.id.textViewIP);

        // Fetch and Display IP Address
        textIP.setText(getIpAddress());


        // This Code is Broken. To be Fixed in the Future..
        // Determine TextView Size for Automatic Scroll
        int scrollAmount;
        try {
            scrollAmount = textStatus.getLayout().getLineTop(textStatus.getLineCount() - textStatus.getHeight());
        } catch (Exception exception) {
            scrollAmount = 1;
        }
        if (scrollAmount > 0) {
            textStatus.scrollTo(0, scrollAmount);
        } else {
            textStatus.scrollTo(0, 0);
        }
    }

    public static String getIpAddress() {
        String ipAddress = "Unable to Fetch IP..";
        try {
            Enumeration en;
            for (en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface)en.nextElement();
                for (Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = (InetAddress)enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()&&inetAddress instanceof Inet4Address) {
                        ipAddress=inetAddress.getHostAddress().toString();
                        return ipAddress;
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return ipAddress;
    }

}
