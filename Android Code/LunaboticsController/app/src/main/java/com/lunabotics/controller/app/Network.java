package com.lunabotics.controller.app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by Logan on 2/18/14.
 * <p/>
 * Thread to Control Network Communication with Control Client
 */
public class Network implements Runnable {

    // Declare Class Variables
    private DatagramSocket socket;
    private volatile boolean autonomy;
    private volatile boolean exitFlag;
    private boolean newData;
    private Handler robotHandler;


    /*
     * Constructor for Network Object
     */
    public Network(int portNumber, Handler robotHandler) {

        // Connect Socket
        networkConnect(portNumber);

        // Initialize Autonomy, Exit Flag, and New Data Flag to False
        autonomy = false;
        exitFlag = false;
        newData = false;

        // Initialize Robot Handler to Pass Commands
        this.robotHandler = robotHandler;
    }

    /*
     * Method to Connect to Control Client over UDP Socket
     */
    public void networkConnect(int portNumber) {

        // Initialize Socket
        try {
            socket = new DatagramSocket(portNumber);
        } catch (SocketException ex) {
            System.out.println("Cannot Create Socket!");
        }

//        // Conduct Handshake
//        if (networkHandshake()){
//            System.out.println("Handshake Successful");
//        } else {
//            System.out.println("Handshake Failed");
//        }

    }

    /*
     * Main Execution Cycle for Thread
     */
    @Override
    public void run() {

        // Loop Network Functionality Indefinitely
        while (true) {

            // Get Data from Socket
            String networkData = networkReceive();

            // Parse Data into Commands
            int[] networkCommands = networkParse(networkData);

            // Create Bundle to Send with Capacity Size Two
            Bundle sendData = new Bundle(2);

            // Add Elements to Bundle
            sendData.putString("ID", "Network");
            sendData.putIntArray("DATA", networkCommands);

            // Send New Data Through Handler
            Message commandMessage = robotHandler.obtainMessage();
            commandMessage.obj = sendData;
            commandMessage.sendToTarget();
        }
    }

    /*
     * Receive Packet Over UDP Socket
     */
    private String networkReceive() {

        // Declare Receiving Packet and Data Buffer
        DatagramPacket packet;
        byte[] packetBuffer = new byte[128];

        packet = new DatagramPacket(packetBuffer, packetBuffer.length);

        // Try-Catch Statement to Receive Data Over Socket
        try {
            socket.receive(packet);
        } catch (IOException ex) {
            System.out.println("No Data Received Over UDP Socket");
        }

        // Receive Socket Data Into Buffer
        byte[] received = new byte[packet.getLength() - 1];
        received = packet.getData();

        // Convert Received Data to String and Remove Empty Characters
        String data = new String(received);
        data = data.replaceAll("\u0000", "");

        // Return String
        return data;
    }

    /*
     * Parse String Data into Commands
     */
    private int[] networkParse(String data) {

        /*
         * Network Packet Structure
         * [0] = Left Side Wheels (-100 - 100)
         * [1] = Right Side Wheels (-100 - 100)
         * [2] = Digging Brush Toggle (0 or 1)
         * [3] = Brush Lift (0 OFF, 1 UP, 2 DOWN)
         * [4] = Bucket Lift (0 OFF, 1 UP, 2 DOWN)
         * [5] = Bucket Pitch (0 OFF, 1 FORWARDS, 2 BACKWARDS)
         * ------------------------------------
         * [6] = Autonomy Status (0 OFF, 1 ON, 2 NORTH, 3 SOUTH, 4 EAST, 5 WEST)
         * [7] = Exit Flag (0 NO EXIT, 1 EXIT)
         */

        // Initialize Network Commands
        int[] networkCommands = new int[8];

        // Check if Data is Autonomy Flag from Client Software
        if (data.equals(new String("a"))) {

            // Set Autonomy Flag
            autonomy = !autonomy;

            // Set Autonomy Bit
            if (autonomy) {
                networkCommands[6] = 1;
            }

        // Check if Data is Exit Flag from Client Software
        } else if (data.equals(new String("e"))) {

            // Set Exit Bit
            if (exitFlag) {
                networkCommands[7] = 1;
            }

            // Check if Data is North Flag
        } else if(data.equals(new String("n"))) {

            // Set Autonomy Flag
            autonomy = !autonomy;

            // Set Autonomy Bit
            if (autonomy) {
                networkCommands[6] = 2;
            }

            // Check if Data is South Flag
        } else if(data.equals(new String("s"))) {

            // Set Autonomy Flag
            autonomy = !autonomy;

            // Set Autonomy Bit
            if (autonomy) {
                networkCommands[6] = 3;
            }

            // Check if Data is East Flag
        } else if(data.equals(new String("ea"))) {

            // Set Autonomy Flag
            autonomy = !autonomy;

            // Set Autonomy Bit
            if (autonomy) {
                networkCommands[6] = 4;
            }

            // Check if Data is West Flag
        } else if(data.equals(new String("w"))) {

            // Set Autonomy Flag
            autonomy = !autonomy;

            // Set Autonomy Bit
            if (autonomy) {
                networkCommands[6] = 5;
            }

        // Parse Data String into Int Array
        } else {

            // Split Data String into Array by Space
            String[] split = data.split("\\ ");

            // Convert String Array to Int Array
            for (int i = 0; i < split.length; i++) {
                networkCommands[i] = Integer.parseInt(split[i]);
            }

            // Set Autonomy Bit (Exit Flag Should Only be Called Once)
            if (autonomy) {
                networkCommands[6] = 1;
            }
        }

        // Return Parsed Network Commands
        return networkCommands;
    }

    /*
     * Conduct Initialization Handshake with Client Code
     */
    private boolean networkHandshake() {

        // Declare Packet and Buffer Size
        DatagramPacket packet;
        byte[] packetBuffer = new byte[32];

        packet = new DatagramPacket(packetBuffer, packetBuffer.length);

        // Try-Catch Statement to Receive Data Over Socket
        try {
            socket.receive(packet);
        } catch (IOException ex) {
            System.out.println("No Data Received Over UDP Socket");
        }

        // Receive Socket Data Into Buffer
        byte[] received = new byte[packet.getLength() - 1];
        received = packet.getData();

        // Convert Received Data to String and Remove Empty Characters
        String data = new String(received);
        data = data.replaceAll("\u0000", "");

        // Check for Handshake Symbol
        if (data.equals("S")){

            // Create Packet with Reply Message 'A'
            String reply = "A";
            byte[] replyBytes = reply.getBytes();
            DatagramPacket replyPacket = new DatagramPacket(replyBytes, replyBytes.length, packet.getAddress(), packet.getPort());

            // Send Reply Packet
            try {
                socket.send(replyPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;

        } else
            return false;
    }
}
