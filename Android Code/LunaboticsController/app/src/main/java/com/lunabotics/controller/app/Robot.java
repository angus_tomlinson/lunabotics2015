package com.lunabotics.controller.app;


import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by logan on 2/19/14.
 * <p/>
 * Main Robot Control Thread. Calls All Robot Functions
 */
public class Robot implements Runnable {

    /*
     * Declare Class Variables for Needed Functions
     */
    Network network;
    Autonomy autonomy;
    Thread networkThread;
    Thread autonomyThread;
    UsbManager manager;
    UsbSerialDriver motorArduino;
    Handler displayHandler;
    Handler robotHandler;
    boolean autonomyActive;

    int weightCollected;


    /*
     * Constructor for Robot Class
     * Initialize Network and Autonomy Settings
     */
    public Robot(Context context, Handler displayHandler) {

        // Initialize Autonomy Booleans
        autonomyActive = false;

        // Connect to Motor Arduino
        try {
            manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            motorArduino = UsbSerialProber.findFirstDevice(manager);
        } catch (Exception ex) {
            // Catch Exception if USB NOT Connected
        }

        // Open Serial Connection to Arduino and Set Baud Rate with Additional Parameters
        try {
            motorArduino.setParameters(9600, UsbSerialDriver.DATABITS_8, UsbSerialDriver.STOPBITS_1, UsbSerialDriver.PARITY_NONE);
            motorArduino.open();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Initialize Handler for Display
        this.displayHandler = displayHandler;

    }


    /*
     * Main Execution Cycle for Thread
     */
    public void run() {

        // Prepare Looper
        Looper.prepare();

        // Initialize Handler
        robotHandler = new Handler() {
            @Override
            public void handleMessage(Message commandMessage) {

                // Declare Thread Identifier Variable and Movement Command Array
                String threadID;
                int[] robotCommands;

                // Read and Parse Data From Bundle
                Bundle receiveData = (Bundle) commandMessage.obj;
                threadID = receiveData.getString("ID");
                robotCommands = receiveData.getIntArray("DATA");

                /*
                 * Parse Packets from Network Thread
                 */
                if (threadID.equals("Network") && !autonomyActive) {

                    // Check for Autonomy Bits
                    if (robotCommands[6] == 1 || robotCommands[6] == 2 || robotCommands[6] == 3 || robotCommands[6] == 4 || robotCommands[6] == 5) {
                        autonomyActive = true;
                        autonomy.autonomyToggle(autonomyActive, robotCommands[6]);
                        statusPrint("Autonomy Active");
                    }

                    // Check for Exit Bit
                    if (robotCommands[7] == 1) {
                        robotTerminate();
                    }

                    // Isolate Movement Commands and Move Robot
                    robotMove(Arrays.copyOfRange(robotCommands, 0, 6));

                } else if (threadID.equals("Network") && autonomyActive) {

                    // Check if Autonomy Has Been Disabled, Otherwise Ignore Packet
                    if (robotCommands[6] == 0) {
                        autonomyActive = false;
                        autonomy.autonomyToggle(autonomyActive, robotCommands[6]);
                        statusPrint("Autonomy Disabled");

                        // Isolate Movement Commands and Move Robot
                        robotMove(Arrays.copyOfRange(robotCommands, 0, 6));
                    }

                    // Check for Exit Bit
                    if (robotCommands[7] == 1) {
                        robotTerminate();
                    }
                }

                /*
                 * Parse Packets from Autonomy Thread
                 */
                if (threadID.equals("Autonomy")  && autonomyActive) {

                    // Update Weight Value
                    weightCollected = robotCommands[6];

                    // Isolate Movement Commands and Move Robot
                    robotMove(Arrays.copyOfRange(robotCommands, 0, 6));

                } else if (threadID.equals("Autonomy") && !autonomyActive) {

                    // Update Weight Value
                    weightCollected = robotCommands[6];
                }

            }
        };

        // Declare Network Socket
        int networkPortNumber = 5020;
        network = new Network(networkPortNumber, robotHandler);
        networkThread = new Thread(network, "Network");
        networkThread.start();

        // Declare Autonomy Socket
        int autonomyPortNumber = 5030;
        autonomy = new Autonomy(autonomyPortNumber, robotHandler);
        autonomyThread = new Thread(autonomy, "Autonomy");
        autonomyThread.start();

        // Start Looper
        Looper.loop();
    }

    /*
     * Send Status Commands Through Handler to Print on Screen
     */
    private void statusPrint(String statusMessage) {

        Message sendMessage = displayHandler.obtainMessage();
        sendMessage.obj = statusMessage;
        sendMessage.sendToTarget();
    }

    /*
     * Method to Move Robot Motors Through Arduino
     */
    private synchronized void robotMove(int[] robotCommands) {

        // Convert Robot Commands to String to Send
        String sendString = "";
        for (int i = 0; i < robotCommands.length; i++) {
            sendString = (sendString + robotCommands[i] + " ");
        }

        // Add Terminating Character
        sendString = sendString + "X";

        // Encode String to Byte Array
        byte[] sendData = null;
        try {
            sendData = sendString.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Receive Send Signal from Arduino
        byte[] received = new byte[1];
        try {
            motorArduino.read(received, 50);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Convert Received Data to String and Remove Empty Characters
        String data = new String(received);
        data = data.replaceAll("\u0000", "");
        boolean found = data.matches("G");

        // Send Commands to Arduino
        try {
            // Check if Character from Arduino is Found
            if (found) {
                motorArduino.write(sendData, 250);
            }
        } catch (IOException e) {
            // Nothing to Do
        }

    }

    /*
     * Method to Terminate Connections and Exit Software
     */
    private void robotTerminate() {

        // Stop All Motors
        robotMove(new int[]{0,0,0,0,0,0});

        // Join Threads
        autonomy.autonomyToggle(false, 0);
        try {
            autonomyThread.join();
            networkThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Send Exit Message
        statusPrint("Exit Program");
    }
}
