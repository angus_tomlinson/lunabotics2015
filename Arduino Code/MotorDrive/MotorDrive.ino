
#include "BMSerial.h"
#include "RoboClaw.h"

/*
 * Receive Commands from Android Controller to Drive Rover
 */

// Define Motor Controller Address
#define address 0x80

// Declare Serial Connection for Each Side
RoboClaw bucketControl(4,5,10000);  // M1 - Actuators, M2 - Winch
RoboClaw diggingBrush(6,7,10000);  // M1 - Raise and Lower Brush, M2 - Spin Brush
RoboClaw leftSide(8,9,10000);
RoboClaw rightSide(10,11,10000);

// Declare Pin Assignments for Reflex Limit Switches
int brushLimitSwitch = A14;
int winchLimitSwitch = A15;

int led = 13;

// Declare Booleans for Ramping Functionality
boolean brushMoving;

// Declare Booleans for Limit Switches
boolean brushLimitStatus;
boolean winchLimitStatus;

// Declare Counter for Limit Switches
int brushLimitCounter;
int winchLimitCounter;

void setup() {
  // Initialize Serial Connections
  bucketControl.begin(9600);
  leftSide.begin(9600);
  rightSide.begin(9600);
  diggingBrush.begin(9600);

  // Initialize USB Serial
  Serial.begin(9600);
  
  // Set Serial Timeout
  Serial.setTimeout(250);

  // Initialize Ramping Booleans
  brushMoving = false;

  // Initialize Pin 13 LED for Overflow Testing
  pinMode(led, OUTPUT);
  
  // Initialize Pins for Limit Switches
  //pinMode(brushLimitSwitch, INPUT);
  //pinMode(winchLimitSwitch, INPUT);
  
  // Initialize Limit Switch Booleans
  brushLimitStatus = false;
  winchLimitStatus = false;
  
  // Initialize Limit Switch Counters
  brushLimitCounter = 0;
  winchLimitCounter = 0;
  
}

void loop() {

  // Delare Int Array to Hold Commands from Android Serial Connection
  int commands[6];

  // Declare Stopping Condition for Loop and Array Position Tracker
  int i = 0;

  // Declare Integer to Store Command Variable
  int dataInt = 0;

  // Declare boolean to Hold Negative Flag
  boolean negative = false;
  
  // Check for Limit Switches
  if(analogRead(brushLimitSwitch) > 1000){
    
    // Test for Counter Value
    if(brushLimitCounter >= 1){
      brushLimitStatus = true;
    } else {
      brushLimitCounter++;
    }
  } else{
    brushLimitStatus = false;
    brushLimitCounter = 0;
  }
  if(analogRead(winchLimitSwitch) > 1000){
    
    // Test for Limit Switches
    if(winchLimitCounter >= 1){
      winchLimitStatus = true;
    } else {
      winchLimitCounter++;
    }
  } else{
    winchLimitStatus = false;
    winchLimitCounter = 0;
  }

  // Acknowlege Ready to Read
  Serial.print('G');

  // Test for Buffer Overflow
  if(Serial.available() >= 63){
    digitalWrite(led, HIGH);
  }

  // Read Bytes from Serial Connection
  char varBuffer[32];
  int numRead = Serial.readBytesUntil('X', varBuffer, 32);

  // Read Data if Available
  if(numRead > 0){

    // Test for Buffer Overflow
    if(Serial.available() >= 63){
      digitalWrite(led, HIGH);
    }

    // Loop for Number of Characters Received EXCLUDING the Terminator Character 'X'
    for(int k = 0; k < numRead; k++) {

      // Declare var char to parse
      char var = varBuffer[k];

      // Add to Data String if Not Space or Terminating Character
      if(var != ' ' && var != 'X' && var != '-') {
        char charHold;
        charHold = var;
        int intHold = charHold - '0';
        dataInt = (dataInt * 10) + intHold;

      } 
      else if(var == ' '){
        // Check if Negative Number
        if(negative){
          dataInt = -dataInt;
        }
        
        // Add to Integer Array and Increment Array Counter
        commands[i] = dataInt;
        i++;
        
        // Reset Variables and Flags
        dataInt = 0;
        negative = false;

      } 
      else if(var == '-'){
        // Set Negative Flag
        negative = true;

      } 
    }
    
    // Send Speeds from Commands Array to Motors
    wheelParse(leftSide, commands[0]);
    wheelParse(rightSide, commands[1]);
    brushDigParse(commands[2]);
    brushLiftParse(commands[3]);
    bucketLiftParse(commands[4]);
    bucketPitchParse(commands[5]);
  } 
}

/*
 * Method to Parse and Drive Wheels from Speed Command 
 */
void wheelParse(RoboClaw wheelSide, int speedValue) {

  // Parse Wheel Speed Value
  int channelSpeed = abs((float)speedValue * 1.27);

  // If Speed is Positive, Drive Forwards
  if(speedValue >= 0) {
    wheelSide.BackwardM1(address, channelSpeed);
    wheelSide.ForwardM2(address, channelSpeed);
  } 
  else {
    // Drive Backwards
    wheelSide.ForwardM1(address, channelSpeed);
    wheelSide.BackwardM2(address, channelSpeed);
  }
}

/*
 * Method to Parse and Drive Digging Brush
 */
void brushDigParse(int brushOn) {

  // Keep Brush OFF if Bit is NOT Set and brushMoving is false
  if(brushOn == 0 && brushMoving == false) {
    // No New Commands to Send
  }
  // Turn Brush ON with Ramping if Bit is Set and brushMoving is false
  else if(brushOn == 1 && brushMoving == false) {
 
    // Set Brush to Quarter Speed
    diggingBrush.ForwardM2(address, 32);
    delay(150);

    // Set Brush to Half Speed
    diggingBrush.ForwardM2(address, 64);
    //delay(150);

    // Set Brush to Full Speed
    diggingBrush.ForwardM2(address, 96);

    // Set brushMoving to true
    brushMoving = true;
  }

  // Keep Brush ON if Bit is Set and brushMoving is true
  else if(brushOn == 1 && brushMoving == true) {
    // No New Commands to Send
  }

  // Turn Brush OFF with Ramping if Bit is NOT Set and brushMoving is true
  else if(brushOn == 0 && brushMoving == true) {

    // Set Brush to Half Speed
    diggingBrush.ForwardM2(address, 64);
    delay(150);

    // Set Brush to Quarter Speed
    diggingBrush.ForwardM2(address, 32);
    delay(150);

    // Set Brush to OFF
    diggingBrush.ForwardM2(address, 0);

    // Set brushMoving to false
    brushMoving = false;
  }
}

/*
 * Method to Parse and Drive Brush Motor
 */
void brushLiftParse(int brushMove) {

  // Keep Brush Stationary if Bit is 0
  if(brushMove == 0){
    diggingBrush.BackwardM1(address, 0);
  }

  // Move Brush UP if Bit is 1
  else if(brushMove == 1){
    
    // Test for Brush Limit Switch
    if(brushLimitStatus){
      diggingBrush.ForwardM1(address, 0);
    } else{
      diggingBrush.ForwardM1(address, 64);
    }  
  }

  // Move Brush DOWN if Bit is 2
  else if(brushMove == 2){
    diggingBrush.BackwardM1(address, 50);
  }
}

/*
 * Method to Parse and Drive Bucket Lift
 */
void bucketLiftParse(int bucketMove) {

  // Keep Bucket Stationary if Bit is 0
  if(bucketMove == 0){
    bucketControl.BackwardM2(address, 0);
  }

  // Lift Bucket UP if Bit is 1
  else if(bucketMove == 1){
    
    // Test for Winch Limit Switch
    if(winchLimitStatus){
      bucketControl.ForwardM2(address, 0);
    } else{
      bucketControl.ForwardM2(address, 28);
    }
  }

  // Lift Bucket DOWN if Bit is 2
  else if(bucketMove == 2){
    digitalWrite(led, HIGH);
    bucketControl.BackwardM2(address, 28);
  }
}

/*
 * Method to Parse and Drive Bucket Actuators
 */
void bucketPitchParse(int bucketMove) {
  
   // Keep Bucket Stationary if Bit is 0
  if(bucketMove == 0){
    bucketControl.BackwardM1(address, 0);
  }

  // Pitch Bucket FORWARDS if Bit is 1
  else if(bucketMove == 1){
    bucketControl.ForwardM1(address, 64);
  }

  // Pitch Bucket BACKWARDS if Bit is 2
  else if(bucketMove == 2){
    bucketControl.BackwardM1(address, 64);
  }
}

/*
 * Interrupt Method for Brush Limit Switch
 */
//void brushInterrupt(){
//  
//  // Test if Limit Switch Engaged
//  if(digitalRead(brushLimitSwitch)){
//    
//    // Stop Brush Lift Motor
//    diggingBrush.ForwardM1(address, 0);
//    
//    // Set Brush Limit Status
//    brushLimitStatus = true;
//  } else {
//    
//    // Set Brush Limit Status
//    brushLimitStatus = false;
//  }
//}

/*
 * Interrupt Method for Winch Limit Switch
 */
//void winchInterrupt(){
//  
//  // Test if Limit Switch Engaged
//  if(digitalRead(winchLimitSwitch)){
//    
//    // Stop Brush Lift Motor
//    bucketControl.ForwardM2(address, 0);
//    
//    // Set Brush Limit Status
//    winchLimitStatus = true;
//  } else {
//    
//    // Set Brush Limit Status
//    winchLimitStatus = false;
//  }
//}




