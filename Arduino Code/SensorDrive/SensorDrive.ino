`  J

/*
 * Receive Commands from Tango to grab data from sensors.
 */

// Define Motor Controller Address.
#define address 0x80

////////////////////////////////////////////////////
//      Analog Weight Sensor Initialization       //
////////////////////////////////////////////////////      
int weightSensor_Front = 0; 
int weightSensor_Back = 2;

double weight_F_init = 0;
double weight_B_init = 0;

////////////////////////////////////////////////////
//      Switch and Bumper Initialization          //
////////////////////////////////////////////////////

const int switch_Front = 8;
const int switch_Back = 9;
const int bumper_Left = 10;    
const int bumper_Right = 11;

int bumper_Left_State = 0;         
int bumper_Right_State = 0; 
int switch_Front_State = 0;
int switch_Back_State = 0;

// Int representing LED. 
int led = 13;
boolean zero = false;
int count = 0;
const int maxCount = 10;
int weightArrayF[maxCount];
int weightArrayB[maxCount];

/**
*
*/
void setup() {
  
  // Initialize USB Serial.
  Serial.begin(9600);
  // Initialize Range Finder Serial.
  Serial1.begin(115200);
  Serial2.begin(115200);
  Serial3.begin(115200);
  
  // Set Serial Timeout (YOU MAY CHANGE).
  //Serial.setTimeout(250);

  // Initialize Pin 13 LED for Overflow Testing.
  pinMode(led, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(bumper_Left, INPUT);    
  pinMode(bumper_Right, INPUT); 
  pinMode(switch_Front, INPUT);
  pinMode(switch_Back, INPUT);
}

/**
*
*/
void loop() {
  
  if(!zero && count <= maxCount){
     weightArrayF[count] = analogRead(weightSensor_Front);
     weightArrayB[count] = analogRead(weightSensor_Back);
     count++;
  } else if (count == maxCount){
     int b = 0;
     int f = 0;
     for(int i = 0; i < maxCount; i++){
       b += weightArrayB[i];
       f += weightArrayF[i];
     }
     weight_F_init = f / maxCount;
     weight_B_init = b / maxCount;
     count = 0;
     zero = true; 
  }
  
  String data = "-1";
  
  // Test for Buffer Overflow.
  if(Serial.available() >= 63){
    digitalWrite(led, HIGH);
  }

  // Read Bytes from Serial Connection.
  char varBuffer[2];
  int numRead = Serial.readBytesUntil('X', varBuffer, 2);

  // Read Data if Available.
  if(numRead > 0){

    // Test for Buffer Overflow.
    if(Serial.available() >= 63){
      digitalWrite(led, HIGH);
    }
    
    // Loop for Number of Characters Received EXCLUDING the Terminator Character 'X'.
    // NUMREAD SHOULD = 1 but this loop is here in case we need to send longer commands..
    for(int k = 0; k < numRead; k++) {

      // Declare var char to parse.
      char var = varBuffer[k];
      
      // If var is 1, 2, 3, or 4.  
      if(var == '1'){
        data = "780 5000 2500";//getRangeFinderData();
      } else if(var == '2'){
        data = "15 15";//getWeightData();
      } else if(var == '3'){
        data = "0";//getBucketPosition();
      } else if(var == '4'){
        data = "0";//getBumperPressed();
      } else if(var == '0'){
        zero = false;
      }
    }
    
    data = data + "\n";
    Serial.print(data);
  } 
}

/*
 * Method to grab Laser Range Finder Data.
 * Returns a string in the format of "[left_sensor_dist] [left_sensor_dist] [left_sensor_dist]".
 */
String getRangeFinderData() {
    int dist_mm;
    //int dist_m;
    String data = "";
    int serial_port;
    
             
            
             if (Serial1.available()>0) // if   Serial1 is available
             {
               serial_port = 1;                                                                              
               dist_mm = getdist_new(serial_port);     //      Get distance from laser range finder
               data = data + dist_mm + " ";
               //dist_m = dist_mm / 1000;                //      Convert distance to meters
             } else{
               data = data + "-1 ";
             }  
             
             if (Serial2.available()>0)   // if   Serial1 is available
             {
               serial_port = 2;                                                    
               dist_mm = getdist(serial_port);             //      Get distance from laser range finder
               data = data + dist_mm + " ";
               //dist_m = dist_mm / 1000;                    //      Convert distance to meters
             } else{
               data = data + "-1 ";
             }  
             
             if (Serial3.available()>0)
             {
               serial_port = 3;
               dist_mm = getdist_new(serial_port);
               data = data + dist_mm;
               //dist_m = dist_mm / 1000;
               //snprintf_P(buf, sizeof(buf), PSTR("Laser distance 3: %d.%dm"), dist_m, dist_mm % 1000);
            
             } else{
                data = data + "-1";
             }  
             
    
}
/*
 * Method to grab Weight Sensor data.
 * Returns a string in the format of "[front_sensor_weight] [back_sensor_weight]".
 */
String getWeightData() {
             int f = analogRead(weightSensor_Front);
             int b = analogRead(weightSensor_Back);
             
             int kg_Front = (f - weight_F_init) / 60;
             int kg_Back = (b  - weight_B_init) / 60;
             
             String data = String(kg_Front);
             data = data + " " + kg_Back;
             return data;
}

/*
 * Method to grab the bucket position.
 * Returns a string in the format of "[number]".
 * "0" for down, "1" for sorta up, "2" for all the way up.
 */
String getBucketPosition() {
	switch_Front_State = digitalRead(switch_Front);
    switch_Back_State  = digitalRead(switch_Back);
	
	// if it is, the buttonState is HIGH:
	if (switch_Back_State == HIGH)  { 
		return "0"; 
	} else if (switch_Front_State != HIGH && switch_Back_State != HIGH) { 
		return "2"; 
	} else {
		return "1";
	}
}

/*
 * Method to grab the button pressed.
 * Returns a string in the format of "[number]".
 * "0" for not, "1" for pressed.
 */
String getBumperPressed() {
	bumper_Left_State  = digitalRead(bumper_Left);
    bumper_Right_State = digitalRead(bumper_Right);
	
	// if it is, the buttonState is HIGH:
	if (bumper_Left_State == HIGH || bumper_Right_State == HIGH)  { 
		return "1";
	} else { 
		return "0"; 
	} 
}

////////////////////////////////////////////////////
//         Laser Range Finder Functions           //
////////////////////////////////////////////////////    
int strstart_P(const char *s1, const char * PROGMEM s2)
{
    return strncmp_P(s1, s2, strlen_P(s2)) == 0;
}

int getdist(int serial_port)
{
    char buf[64];
    char *comma;
    int dist;
    int rc;
    
    if(serial_port == 1) { Serial1.write("*00004#"); }    
    if(serial_port == 2) { Serial2.write("*00004#"); }   
    if(serial_port == 3) { Serial3.write("*00004#"); }

    int counter = 0;
    for (;;) {
        
        if(serial_port == 1) { rc = Serial1.readBytesUntil('\n', buf, sizeof(buf)); }               // Searches for '\n' then reads "sizeof(buf)" bytes and stores them to "buf"              
        if(serial_port == 2) { rc = Serial2.readBytesUntil('\n', buf, sizeof(buf)); }   
        if(serial_port == 3) { rc = Serial3.readBytesUntil('\n', buf, sizeof(buf)); } 
        
        buf[rc] = '\0';
        counter++;

        if (!strstart_P(buf, PSTR("Dist: "))) {        // if   contents of "buf" do not begin with "Dist: "
            if(counter<20)
            {
              continue;      
            }else{
              return -1;
            }
        }

        comma = strchr(buf, ',');
        if (comma == NULL)  {                                                                        // if there is no comma in "buf"
            continue;  
        }            // then a null pointer is returned. Restart the for loop

        *comma = '\0';                                                                              // put end of string character in "buf" at the location that comma points to
        
        dist = atoi(buf + strlen_P(PSTR("Dist: ")));                                                // take the contents of "buf" from "buf[6]" onward until end of line character '\0' is reached and convert the char representation of the number and convert it to an integer
        return dist;                                                                                // atoi takes a string of numbers represented by char's and converts it into an integer
    }
}

int getdist_new(int serial_port)
{
    char buf[64];
    char buf_new[5];
    char *comma;
    char *pound;
    int dist;
    int rc;
    
    if(serial_port == 1) {  Serial1.write("*00004#"); }    
    if(serial_port == 2) {  Serial2.write("*00004#"); } 
    if(serial_port == 3) {  Serial3.write("*00004#"); }  

    int counter = 0;
    for (;;) {
        
        if(serial_port == 1) { rc = Serial1.readBytesUntil('\n', buf, sizeof(buf)); }               // Searches for '\n' then reads "sizeof(buf)" bytes and stores them to "buf"      
        if(serial_port == 2) { rc = Serial2.readBytesUntil('\n', buf, sizeof(buf)); }  
        if(serial_port == 3) { rc = Serial3.readBytesUntil('\n', buf, sizeof(buf)); }  
        
        buf[rc] = '\0';           // puts '\0' in "buf" at the end of the read in information
        counter++;

        if (!strstart_P(buf, PSTR("*"))) {      
            if(counter<20)
            {
              continue;      
            }else{
              return -1;
            }
        }                

        comma = strchr(buf, '#');                                                                   // comma points to location in "buf" containing the '#' char
        if (comma == NULL)   {                                                                       // if there is no comma in "buf"
            continue;     
        }            // then a null pointer is returned. Restart the for loop

        *comma = '\0';

        int n0 = *(comma-3);
        int n1 = *(comma-4);
        int n2 = *(comma-5);
        int n3 = *(comma-6);
        int n4 = *(comma-7);
        
        buf_new[0] = n4;
        buf_new[1] = n3;
        buf_new[2] = n2;
        buf_new[3] = n1;
        buf_new[4] = n0;
       
        dist = atoi(buf_new);
        return dist;                                                                                // x is a memory location
    }
}



