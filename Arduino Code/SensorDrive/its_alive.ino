const int READ_TIME = 100;
#include <string.h>
void setup() {
  // initilize USB Serial
  Serial.begin(9600);
  //initialize rangefinder serial
  Serial1.begin(115200);
  //Serial1.setTimeout(1000);
}

void loop() {
  delay(500);
  requestRead();
  String data = readMemory();
  transmit(data);
  delay(500);
}

void requestRead() {
  Serial1.write("*00004#");//signal to request read
  transmit("0");
}

String readMemory() {
  int pointless;
  char buf[64];
  int counter;
  //do while the contents of buf do not begin with "Dist:"
  do{
    transmit("Reading");
    
    pointless = Serial1.readBytesUntil('\n', buf, sizeof(buf)); // Searches for '\n' then reads "sizeof(buf)" bytes and stores them to "buf
  } while(counter++ < READ_TIME && !strstart_P(buf, PSTR("Dist: ")));

  if(!strstart_P(buf, PSTR("Dist: "))) {
    return "NO DATA";
  } else{
     return buf; 
  }
}

void transmit(String str) {
  Serial.println(str);
}


////////////////////////////////////////////////////////stolen code
int strstart_P(const char *s1, const char * PROGMEM s2)
{
  return strncmp_P(s1, s2, strlen_P(s2)) == 0;
}



//possibly useful for the red range finders.
int getdist_new(int serial_port)
{
    char buf[64];
    char buf_new[5];
    char *comma;
    char *pound;
    int dist;
    int rc;
    
    if(serial_port == 1) {  Serial1.write("*00004#"); }    
    if(serial_port == 2) {  Serial2.write("*00004#"); } 
    if(serial_port == 3) {  Serial3.write("*00004#"); }  

    int counter = 0;
    for (;;) {
        
        if(serial_port == 1) { rc = Serial1.readBytesUntil('\n', buf, sizeof(buf)); }               // Searches for '\n' then reads "sizeof(buf)" bytes and stores them to "buf"      
        if(serial_port == 2) { rc = Serial2.readBytesUntil('\n', buf, sizeof(buf)); }  
        if(serial_port == 3) { rc = Serial3.readBytesUntil('\n', buf, sizeof(buf)); }  
        
        buf[rc] = '\0';           // puts '\0' in "buf" at the end of the read in information
        counter++;

        if (!strstart_P(buf, PSTR("*"))) {      
            if(counter<20)
            {
              continue;      
            }else{
              return -1;
            }
        }                

        comma = strchr(buf, '#');                                                                   // comma points to location in "buf" containing the '#' char
        if (comma == NULL)   {                                                                       // if there is no comma in "buf"
            continue;     
        }            // then a null pointer is returned. Restart the for loop

        *comma = '\0';

        int n0 = *(comma-3);
        int n1 = *(comma-4);
        int n2 = *(comma-5);
        int n3 = *(comma-6);
        int n4 = *(comma-7);
        
        buf_new[0] = n4;
        buf_new[1] = n3;
        buf_new[2] = n2;
        buf_new[3] = n1;
        buf_new[4] = n0;
       
        dist = atoi(buf_new);
        return dist;                                                                                // x is a memory location
    }
}


