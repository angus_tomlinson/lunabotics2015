﻿namespace LunaWinForm
{
    partial class MAMBAControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MAMBAControl));
            this.buttonConnectRobot = new System.Windows.Forms.Button();
            this.timerUpdateMotors = new System.Windows.Forms.Timer(this.components);
            this.textBoxNetAddress = new System.Windows.Forms.TextBox();
            this.labelIP = new System.Windows.Forms.Label();
            this.buttonDisconnectRobot = new System.Windows.Forms.Button();
            this.ipLabel = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.xboxControllerLabel = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.xboxControllerStatus = new System.Windows.Forms.Label();
            this.diggingWheelLabel = new System.Windows.Forms.Label();
            this.diggingWheelStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonConnectRobot
            // 
            this.buttonConnectRobot.Location = new System.Drawing.Point(231, 7);
            this.buttonConnectRobot.Name = "buttonConnectRobot";
            this.buttonConnectRobot.Size = new System.Drawing.Size(59, 23);
            this.buttonConnectRobot.TabIndex = 0;
            this.buttonConnectRobot.Text = "Connect";
            this.buttonConnectRobot.UseVisualStyleBackColor = true;
            this.buttonConnectRobot.Click += new System.EventHandler(this.buttonConnectRobot_Click);
            // 
            // textBoxNetAddress
            // 
            this.textBoxNetAddress.Location = new System.Drawing.Point(67, 9);
            this.textBoxNetAddress.Name = "textBoxNetAddress";
            this.textBoxNetAddress.Size = new System.Drawing.Size(158, 20);
            this.textBoxNetAddress.TabIndex = 6;
            this.textBoxNetAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelIP
            // 
            this.labelIP.AutoSize = true;
            this.labelIP.Location = new System.Drawing.Point(13, 16);
            this.labelIP.Name = "labelIP";
            this.labelIP.Size = new System.Drawing.Size(52, 13);
            this.labelIP.TabIndex = 7;
            this.labelIP.Text = "Robot IP:";
            // 
            // buttonDisconnectRobot
            // 
            this.buttonDisconnectRobot.Location = new System.Drawing.Point(296, 7);
            this.buttonDisconnectRobot.Name = "buttonDisconnectRobot";
            this.buttonDisconnectRobot.Size = new System.Drawing.Size(75, 23);
            this.buttonDisconnectRobot.TabIndex = 14;
            this.buttonDisconnectRobot.Text = "Disconnect";
            this.buttonDisconnectRobot.UseVisualStyleBackColor = true;
            this.buttonDisconnectRobot.Click += new System.EventHandler(this.buttonDisconnectRobot_Click);
            // 
            // ipLabel
            // 
            this.ipLabel.AutoSize = true;
            this.ipLabel.Location = new System.Drawing.Point(381, 11);
            this.ipLabel.Name = "ipLabel";
            this.ipLabel.Size = new System.Drawing.Size(35, 13);
            this.ipLabel.TabIndex = 15;
            this.ipLabel.Text = "NULL";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(596, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(52, 19);
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // xboxControllerLabel
            // 
            this.xboxControllerLabel.AutoSize = true;
            this.xboxControllerLabel.BackColor = System.Drawing.Color.Transparent;
            this.xboxControllerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xboxControllerLabel.Location = new System.Drawing.Point(13, 96);
            this.xboxControllerLabel.Name = "xboxControllerLabel";
            this.xboxControllerLabel.Size = new System.Drawing.Size(108, 17);
            this.xboxControllerLabel.TabIndex = 18;
            this.xboxControllerLabel.Text = "Xbox Controller:";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.Location = new System.Drawing.Point(13, 59);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(186, 18);
            this.statusLabel.TabIndex = 19;
            this.statusLabel.Text = "Current MAMBA Status:";
            // 
            // xboxControllerStatus
            // 
            this.xboxControllerStatus.AutoSize = true;
            this.xboxControllerStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.xboxControllerStatus.Location = new System.Drawing.Point(127, 100);
            this.xboxControllerStatus.Name = "xboxControllerStatus";
            this.xboxControllerStatus.Size = new System.Drawing.Size(73, 13);
            this.xboxControllerStatus.TabIndex = 20;
            this.xboxControllerStatus.Text = "Disconnected";
            // 
            // diggingWheelLabel
            // 
            this.diggingWheelLabel.AutoSize = true;
            this.diggingWheelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diggingWheelLabel.Location = new System.Drawing.Point(13, 127);
            this.diggingWheelLabel.Name = "diggingWheelLabel";
            this.diggingWheelLabel.Size = new System.Drawing.Size(104, 17);
            this.diggingWheelLabel.TabIndex = 21;
            this.diggingWheelLabel.Text = "Digging Wheel:";
            // 
            // diggingWheelStatus
            // 
            this.diggingWheelStatus.AutoSize = true;
            this.diggingWheelStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.diggingWheelStatus.Location = new System.Drawing.Point(127, 129);
            this.diggingWheelStatus.Name = "diggingWheelStatus";
            this.diggingWheelStatus.Size = new System.Drawing.Size(47, 13);
            this.diggingWheelStatus.TabIndex = 22;
            this.diggingWheelStatus.Text = "Stopped";
            // 
            // MAMBAControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 376);
            this.Controls.Add(this.diggingWheelStatus);
            this.Controls.Add(this.diggingWheelLabel);
            this.Controls.Add(this.xboxControllerStatus);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.xboxControllerLabel);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.ipLabel);
            this.Controls.Add(this.buttonDisconnectRobot);
            this.Controls.Add(this.labelIP);
            this.Controls.Add(this.textBoxNetAddress);
            this.Controls.Add(this.buttonConnectRobot);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MAMBAControl";
            this.Text = "MAMBA Client Control Center";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonConnectRobot;
        private System.Windows.Forms.Timer timerUpdateMotors;
        private System.Windows.Forms.TextBox textBoxNetAddress;
        private System.Windows.Forms.Label labelIP;
        private System.Windows.Forms.Button buttonDisconnectRobot;
        private System.Windows.Forms.Label ipLabel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label xboxControllerLabel;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label xboxControllerStatus;
        private System.Windows.Forms.Label diggingWheelLabel;
        private System.Windows.Forms.Label diggingWheelStatus;
    }
}