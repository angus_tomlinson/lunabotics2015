﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using SlimDX;
using SlimDX.XInput;
using System.Windows.Input;

namespace LunaWinForm
{
    public class Control
    {
        // Declare Class Variables
        public Socket sending_socket;
        private IPAddress send_to_address;
        private IPEndPoint sending_end_point;

        private string[] packet;

        uint lastPacket;

        public Controller controller;
        public ThumbstickState LeftStick { get; private set; }
        public ThumbstickState RightStick { get; private set; }
        public bool A { get; private set; }
        public bool X { get; private set; }
        public bool B { get; private set; }
        public bool Y { get; private set; }
        public bool BACK { get; private set; }
        public bool START { get; private set; }
        public bool LEFTTRIGGER { get; private set; }
        public bool RIGHTTRIGGER { get; private set; }
        public bool LEFTBUMPER { get; private set; }
        public bool RIGHTBUMPER { get; private set; }
        public bool UP { get; private set; }
        public bool DOWN { get; private set; }
        public bool LEFT { get; private set; }
        public bool RIGHT { get; private set; }

        public bool last = false;

        public bool autonomy = false;
        public bool diggingToggle = false;
        public bool conveyorKey = false;
        public bool scissorUpKey = false;
        public bool scissorDownKey = false;

        /*
         * Method to Connect to UDP Socket and Game Controller
         */ 
        public Control(string ip)
        {
            // Connect to Socket
            sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            send_to_address = IPAddress.Parse(ip);
            sending_end_point = new IPEndPoint(send_to_address, 5020);

            // Initialize Packet Structure and Set Correct Size
            packet = new string[6];

            // Connect to Game Controller
            try
            {
                controller = new Controller(0);
            }
            catch (Exception e)
            {

            }
        }

        /*
         * Method to Normalize Game Controller Input
         */ 
        public Vector2 Normalize(short rawX, short rawY, short threshold)
        {
            var value = new Vector2(rawX, rawY);
            var magnitude = value.Length();
            var direction = value / (magnitude == 0 ? 1 : magnitude);

            var normalizedMagnitude = 0.0f;
            if (magnitude - threshold > 0)
                normalizedMagnitude = Math.Min((magnitude - threshold) / (short.MaxValue - threshold), 1);

            return direction * normalizedMagnitude;
        }

        /*
         * Method to Record New Game Controller State and Send Control Packet Over Network
         */ 
        public bool update()
        {
            // Capture Current State of Game Controller
            State state = controller.GetState();

            // Prevent Update if No Change has Occured on Game Controller
            if (lastPacket == state.PacketNumber)
                return true;

            // Update Tracking Variable
            lastPacket = state.PacketNumber;

            // Fetch Gamepad State from Game Controller
            var gamepadState = state.Gamepad;

            // Initialize Game Controller Button Booleans
            A = (gamepadState.Buttons & GamepadButtonFlags.A) != 0;
            B = (gamepadState.Buttons & GamepadButtonFlags.B) != 0;
            X = (gamepadState.Buttons & GamepadButtonFlags.X) != 0;
            Y = (gamepadState.Buttons & GamepadButtonFlags.Y) != 0;
            BACK = (gamepadState.Buttons & GamepadButtonFlags.Back) != 0;
            START = (gamepadState.Buttons & GamepadButtonFlags.Start) != 0;
            RIGHTBUMPER = (gamepadState.Buttons & GamepadButtonFlags.RightShoulder) != 0;
            LEFTBUMPER = (gamepadState.Buttons & GamepadButtonFlags.LeftShoulder) != 0;
            UP = (gamepadState.Buttons & GamepadButtonFlags.DPadUp) != 0;
            DOWN = (gamepadState.Buttons & GamepadButtonFlags.DPadDown) != 0;
            LEFT = (gamepadState.Buttons & GamepadButtonFlags.DPadLeft) != 0;
            RIGHT = (gamepadState.Buttons & GamepadButtonFlags.DPadRight) != 0;
            LEFTTRIGGER = (gamepadState.LeftTrigger >= 100) ? true : false;
            RIGHTTRIGGER = (gamepadState.RightTrigger >= 100) ? true : false;

            // Fetch and Normalize Stick Inputs
            LeftStick = new ThumbstickState(Normalize(gamepadState.LeftThumbX, gamepadState.LeftThumbY, Gamepad.GamepadLeftThumbDeadZone));
            RightStick = new ThumbstickState(Normalize(gamepadState.RightThumbX, gamepadState.RightThumbY, Gamepad.GamepadRightThumbDeadZone));

            /*
             * Assign Buttons to Functions and Construct Packet
             */

            // Digging Wheel Toggle
            if (Y)
            {
                diggingToggle = !diggingToggle;
            }

            //// Conveyor Belt Key
            //if (Keyboard.IsKeyDown(Key.Space))
            //{
            //    conveyorKey = true;
            //}

            //// Scissor Lift UP Key
            //if (Keyboard.GetKeyStates(Key.Up).Equals(KeyStates.Down))
            //{
            //    scissorUpKey = true;
            //}

            //// Scissor Lift DOWN Key
            //if (Keyboard.GetKeyStates(Key.Down).Equals(KeyStates.Down))
            //{
            //    scissorDownKey = true;
            //}

            // Assign Wheel Sides Controls
            packet[0] = (int)(100 * LeftStick.getYComponent()) + " ";  // Move Left-Side Wheels
            packet[1] = (int)(100 * RightStick.getYComponent()) + " "; // Move Right-Side Wheels

            // Assign Digging Brush Controls
            packet[2] = (diggingToggle) ? 1 + " " : 0 + " "; // Digging Wheel Spin FORWARDS

            // Assign Brush Screw Controls
            if ((RIGHTBUMPER))
            {
                packet[3] = 1 + " "; // Brush Screw UP
            }
            else if ((LEFTBUMPER))
            {
                packet[3] = 2 + " "; // Brush Screw DOWN
            }
            else
            {
                packet[3] = 0 + " "; // Brush Screw NO MOVEMENT
            }

            // Assign Bucket Lift Controls            
            if (((gamepadState.RightTrigger >= 50) && B && !X && !A) || scissorUpKey)
            {
                packet[4] = 3 + " "; // Bucket Lift SLOW UP
            }
            else if (((gamepadState.LeftTrigger >= 50) && B && !X && !A) || scissorDownKey)
            {
                packet[4] = 4 + " "; // Bucket Lift SLOW DOWN
            }
            else if (((gamepadState.RightTrigger >= 50) && !B && !X && !A) || scissorUpKey)
            {
                packet[4] = 1 + " "; // Bucket Lift UP
            }
            else if (((gamepadState.LeftTrigger >= 50) && !B && !X && !A) || scissorDownKey)
            {
                packet[4] = 2 + " "; // Bucket Lift DOWN
            }
            else
            {
                packet[4] = 0 + " "; // Bucket Lift NO MOVEMENT
            }

            // Assign Bucket Pitch Controls
            if ((X) && (gamepadState.LeftTrigger >= 50))
            {
                packet[5] = 1 + " "; // Bucket Pitch FORWARDS
            }
            else if (((X) && (gamepadState.RightTrigger >= 50)) || conveyorKey)
            {
                packet[5] = 2 + " "; // Bucket Pitch BACKWARDS
            }
            else
            {
                packet[5] = 0 + " "; // Bucket Pitch NO MOVEMENT
            }


            /*
             * Assign Buttons for Autonomy Toggle and Exit
             */ 
            try
            {
                string data = "";
                foreach (string i in packet)
                    data += i;

                // Send Packet to Toggle Autonomy
                if ( START )
                {
                    data = "a";
                    autonomy = !autonomy;
                    byte[] sending = System.Text.Encoding.ASCII.GetBytes(data);
                    sending_socket.SendTo(sending, sending_end_point);
                    return true;
                }

                // Send Packet to Exit
                else if ( BACK )
                {
                    data = "e";
                    Console.WriteLine("e");
                    byte[] sending = System.Text.Encoding.ASCII.GetBytes(data);
                    sending_socket.SendTo(sending, sending_end_point);
                    autonomy = false;
                    return true;
                }
                // Send Packet to Initialize North
                else if (UP)
                {
                    data = "n";
                    autonomy = !autonomy;
                    byte[] sending = System.Text.Encoding.ASCII.GetBytes(data);
                    sending_socket.SendTo(sending, sending_end_point);
                    return true;
                }

                // Send Packet to Initialize South
                else if (DOWN)
                {
                    data = "s";
                    autonomy = !autonomy;
                    byte[] sending = System.Text.Encoding.ASCII.GetBytes(data);
                    sending_socket.SendTo(sending, sending_end_point);
                    return true;
                }

                // Send Packet to Initialize East
                else if (RIGHT)
                {
                    data = "ea";
                    autonomy = !autonomy;
                    byte[] sending = System.Text.Encoding.ASCII.GetBytes(data);
                    sending_socket.SendTo(sending, sending_end_point);
                    return true;
                }

                // Send Packet to Initialize West
                else if (LEFT)
                {
                    data = "w";
                    autonomy = !autonomy;
                    byte[] sending = System.Text.Encoding.ASCII.GetBytes(data);
                    sending_socket.SendTo(sending, sending_end_point);
                    return true;
                }
                else if (!autonomy)
                {
                    //Console.WriteLine("drive");
                    Console.WriteLine(data);
                    byte[] sending = System.Text.Encoding.ASCII.GetBytes(data);
                    sending_socket.SendTo(sending, sending_end_point);
                    return true;
                }
                else
                {
                    Console.WriteLine("Not in Code");
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        /*
         * Main Execution Cycle for Thread
         */
        public void Run()
        {

            // Loop for Duration of Program
            while (true)
            {
                try
                {
                    // Update Commands from Controller and Send over Network
                    update();
                }
                catch (Exception e)
                {
                    // Throw Error Message Dialog
                    DialogResult result = MessageBox.Show("Error Sending or Updating Data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    // Attempt to Reconnect the Game Controller
                    try
                    {
                        controller = new Controller(0);
                    }
                    catch (Exception ex) { }
                 }
            }
        }
    }

    /*
     * Creat Struct to Simplify Access to Thumbstick Data
     */ 
    public struct ThumbstickState
    {
        public readonly Vector2 Position;

        public ThumbstickState(Vector2 position)
        {
            Position = position;
        }

        // Return Y Component
        public float getYComponent()
        {
            return this.Position.Y;
        }

        // Return X Component
        public float getXComponent()
        {
            return this.Position.X;
        }
    }
}