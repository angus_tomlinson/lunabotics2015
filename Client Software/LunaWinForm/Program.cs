﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace LunaWinForm
{
    static class Program
    {
        /// <summary>
        /// Main Entry Point for Application
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Create Application Window
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MAMBAControl());
        }
    }
}