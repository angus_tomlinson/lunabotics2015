﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SlimDX;
using SlimDX.XInput;
using System.Drawing.Drawing2D;
using System.Timers;
using System.Diagnostics;

namespace LunaWinForm
{
    /*
     * Class to Create and Control Interface Window
     */ 
    public partial class MAMBAControl : Form
    {
        // Declare Needed Class Variables
        public Control control;
        public Thread control_thread;
        public Sensor sensor;
        public Thread sensor_thread;
        public Thread update_ip_thread;
        private System.Timers.Timer ip_update_tmr;

        delegate void SetTextCallback(string text);

        /*
         * Constructor to Initialize Window
         */ 
        public MAMBAControl()
        {
            InitializeComponent();

            // Update IP Address on Window
            this.ip_update_tmr = new System.Timers.Timer();
            this.ip_update_tmr.Elapsed += new ElapsedEventHandler(ip_update_tmr_event);	
            this.ip_update_tmr.Interval = 5000;
            this.ip_update_tmr.Enabled = true;

            // Fill Form with a Default Address
            textBoxNetAddress.Text = "192.168.1.101";
        }

        /*
         * Event to Update IP Reset Timer
         */ 
        private void ip_update_tmr_event(object source, ElapsedEventArgs e)
        {
            this.update_ip_thread = new Thread(new ThreadStart(this.ThreadProcSafe));

            this.update_ip_thread.Start();
        }

        /*
         * Thread Safe Method to Fetch IP
         */ 
        private void ThreadProcSafe()
        {
            string strHostName = "", localIP ="";
            strHostName = Dns.GetHostName();

            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);

            foreach (IPAddress ip in ipEntry.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            this.SetText(localIP);
        }

        /*
         * Method to Compare Thread ID of the Calling Thread to the Creating Thread
         * Return True if Different
         */ 
        private void SetText(string text)
        {
            if (this.ipLabel.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.ipLabel.Text = text;
            }
        }

        /*
         * Method to Set Text Box
         */ 
        public void setTextBox(string values)
        {
            if (InvokeRequired)
            {
                SetTextCallback b = new SetTextCallback(SetText);
                this.Invoke(b, new object[] {values}); 
            }
            string[] data = values.Split(',');
            for (int i = 0; i < data.Length; i++)
            {
                Console.WriteLine(data[i]);
            }

            Application.DoEvents();
        }

        /*
         * Method to Connect to Robot
         */ 
        public bool connectToRobot()
        {
            // Create Control and Sensor Threads
            try
            {
                control = new Control(textBoxNetAddress.Text);
                control_thread = new Thread(control.Run);

                sensor = new Sensor();
                sensor_thread = new Thread(sensor.Run);

                sensor_thread.Start();
                while(!sensor_thread.IsAlive);

                control_thread.Start();
                while (!control_thread.IsAlive) ;
            }
            catch (Exception e) { Console.WriteLine(e); return false; }
            return true;
        }

        /*
         * Method to Disconnect from Robot
         */ 
        public bool disconnectFromRobot()
        {
            // Close Control and Sensor Threads
            try
            {
                control.sending_socket.Close();
                control_thread.Abort();
                control_thread.Join();

                sensor.listener.Close();
                sensor_thread.Abort();
                sensor_thread.Join();
            }
            catch (Exception e) { Console.WriteLine(e); return false; }
            return true;
        }

        /*
         * Event Handler for Connect Button
         */ 
        private void buttonConnectRobot_Click(object sender, EventArgs e)
        {
            connectToRobot();
            buttonConnectRobot.Enabled = false;
            buttonDisconnectRobot.Enabled = true;
            ipLabel.Text = "YES.";
        }

        /*
         * Event Handler for Disconnect Button
         */ 
        private void buttonDisconnectRobot_Click(object sender, EventArgs e)
        {
            disconnectFromRobot();
            buttonConnectRobot.Enabled = true;
            buttonDisconnectRobot.Enabled = false;
            ipLabel.Text = "NO.";
        }

        /*
         * Method to Change Text on Window
         */ 
        private void changeText(TextBox toChange, string text)
        {
            toChange.Text = text;
        }
    }
}
