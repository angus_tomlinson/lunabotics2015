﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Timers;

namespace LunaWinForm
{
    public class Video
    {
	    public UdpClient  listener;
	    private IPEndPoint group_ep;

        private byte[] receive_bytes;

        private byte[] data_array;

        PictureBox picturebox;
        PictureBox fpsbox;
        System.Timers.Timer tmr;

        int frame_count;
        
        public Video(PictureBox picturebox, PictureBox fpsbox)
        {
            listener = new UdpClient(9696);
            group_ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9696);

            data_array = new byte[57600];

            this.picturebox = picturebox;
            this.picturebox.Paint += new PaintEventHandler(this.paint_video);
            ((System.ComponentModel.ISupportInitialize)(this.picturebox)).BeginInit();

            this.fpsbox        = fpsbox;
            this.fpsbox.Paint += new PaintEventHandler(this.paint_fps);
            ((System.ComponentModel.ISupportInitialize)(this.fpsbox)).BeginInit();

            this.tmr          = new System.Timers.Timer();
            this.tmr.Elapsed += new ElapsedEventHandler(tmr_event);
            
            this.tmr.Interval =1000;
            this.tmr.Enabled  =true;
        }

        public void Run()
        {
            int packet_count = 0;
            string SYNC = "SYNC_WITH_ME";
            bool sync    = false;
            while (true)
            {
                try
                {
                    receive_bytes = listener.Receive(ref group_ep);
                }
                catch (Exception e) { Console.WriteLine(e); }

                if (receive_bytes.Length == 12)
                {
                    if (System.Text.Encoding.ASCII.GetString(receive_bytes).Equals(SYNC))
                    {
                        sync = true;
                        packet_count = 0;
                    }
                    else
                    {
                        sync = false;
                    }
                }
                else
                {
                    if (sync)
                    {
                        for (int j = 0; j < 28800; j++)
                        {
                            data_array[(28800 * (packet_count) + j)] = receive_bytes[j];
                        }
                        packet_count++;
                    }
                    else
                    {
                        packet_count = 0;
                    }
                }

                if (packet_count == 2 && sync)
                {
                    frame_count++;
                    sync = false;
                    packet_count = 0;
                    update();
                }
            }
        }

        public void update()
        {
            picturebox.Invalidate();
        }

        public void paint_video(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Bitmap bmp = new Bitmap(160, 120);

            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            Marshal.Copy(data_array, 0, bmpData.Scan0, data_array.Length);

            bmp.UnlockBits(bmpData);

            picturebox.Image = new Bitmap(bmp, 640, 480);
        }
        public void paint_fps(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 11))
            {
                e.Graphics.DrawString("FPS " + frame_count.ToString(), myFont, Brushes.Black, new Point(0, 0));
            }

            frame_count = 0;
        }
        private void tmr_event(object source, ElapsedEventArgs e)
        {
            fpsbox.Invalidate();
        }
    }
}
